function setBackground() {
    // $(document).ready(function () {
    var d = new Date();
    var n = d.getHours();

    if (n >= 19 || n < 6)
        // If time is after 7PM or before 7AM, apply night theme to ‘body’
        document.body.className = "night";
    else if (n >= 6 && n < 7)
        // If time is after 6AM and before 7AM, apply morning theme to ‘body’
        document.body.className = "morning";
    // Else use ‘day’ theme
    else document.body.className = "day";

    setTimeout(setBackground, 1000);
}

setBackground()