# daynight_clock

A simple HTML/javascript clock that is made to run on a Raspberry Pi's chromium browser in fullscreen mode. It shows a green background when it's daytime, and a red background when it's nighttime so my kids know when it's ok to wake us up in the morning!

## Prerequisites

* A Raspberry Pi with a standard Raspberry Pi OS install
* A TFT screen. I'm using a [3.5" TFT touchscreen](https://thepihut.com/collections/raspberry-pi-screens/products/abs-case-with-3-5-tft-touchscreen-for-raspberry-pi-4-480x320)

## Installation

1. Rotate the screen orientation to be vertical

```bash
sudo vi /boot/config.txt
```

For my screen, I changed the following line to rotate 180 degrees:

```bash
dtoverlay=tft35a:rotate=180
```

2. Clone this repo:

```bash
cd /home/pi
mkdir src
cd src
git clone https://gitlab.com/petercrocker/daynight_clock.git
```

3. Setup the chromium browser to launch on boot, and load the clock:

```bash
cat <<EOF >> /home/pi/.config/lxsession/LXDE-pi/autostart
@xset s off
@xset -dpms
@xset s noblank
@unclutter -idle 0.1 -root
@chromium-browser --kiosk /home/pi/src/daynight_clock/index.html
EOF
```

## Usage

Once the files are installed, simply reboot!

```bash
sudo reboot now
```
