function showDate() {
    var today = new Date();
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var curWeekDay = days[today.getDay()];
    var curDay = today.getDate();
    var curMonth = months[today.getMonth()];
    var curYear = today.getFullYear();
    var date = curDay + " " + curMonth + " " + curYear;
    document.getElementById("MyDayDisplay").innerHTML = curWeekDay;
    document.getElementById("MyDateDisplay").innerHTML = date;
}

showDate();